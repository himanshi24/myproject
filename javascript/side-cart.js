class AddtoCart {
    constructor() {
        console.log("HEllo");
      this.getSelected();
      this.removeBtn();
      this.quantityBtn();
      this.closeBtn();
    }
    getSelected = () => {
      let option1 = document.querySelector(".option-1 input:checked").value;
      let option2 = document.querySelector(".option-2 input:checked").value;
      let size = document.querySelectorAll('.option-1 input');
      let color = document.querySelectorAll('.option-2 input');
      let quantity = document.querySelector('.quantity input');
      let qtyValue = '1';
      let variant;
      size.forEach((item) => {
        item.addEventListener("change", () => {
          option1 = item.value;
          totalVariant(option1,option2,qtyValue);
        });
      });
      color.forEach((item) => {
        item.addEventListener("change", () => {
          option2 = item.value;
          totalVariant(option1,option2,qtyValue);
        });
      });
      quantity.addEventListener("change", () => {
        qtyValue = quantity.value;
        totalVariant(option1,option2,qtyValue);
      });
      totalVariant = (option1,option2,qtyValue) => {
        for (let i = 0; i < myCurrentProduct.variants.length; i++) {
          let cVariant = myCurrentProduct.variants[i];
          if (option1 == cVariant.option1 && option2 == cVariant.option2) {
            variant = cVariant.id;
          }
        }
        console.log(variant);
        console.log(qtyValue);
        // this.submitEvent(variant,qtyValue);
        let addToCart = document.querySelector('.product-form .form');
        addToCart.addEventListener("submit",(e) => {
          e.preventDefault();
          let formData = {
            'id': variant,
            'quantity': qtyValue
          };
          fetch('/cart/add.js', {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json'
            },
            body: JSON.stringify(formData)
          })
          .then(response => {
            return response.json();
          })
          .then(response => {
           console.log(response);
          })
          .then(response => {
           document.querySelector('.sidecart-wrapper').classList.add('active');
          })
          .catch((error) => {
            console.error('Error:', error);  
          });
        });
      };
      totalVariant(option1,option2,qtyValue);
    };
    removeBtn = () => {
      let remove = document.querySelectorAll('a.remove-btn');
      remove.forEach(removeBtn => {
        removeBtn.addEventListener("click", (e) => {
          e.preventDefault();
          var productid = removeBtn.getAttribute('data-id');
          let formData= {
            'id': productid,
            'quantity':0
          }
          fetch("/cart/change.js",{
            method:"POST",
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify(formData)
          })
          .then( response => {
            return response.json();
          })
          .then( response => {
            console.log(response);
            console.log("removed succsessfully");
          })
        })
      })
    }
    quantityBtn = () => {
      let plus = document.querySelectorAll("li .plus");
      plus.forEach(p => {
        p.addEventListener("click",(e) => {
          e.preventDefault();
          let x = p.nextElementSibling.value;
          p.nextElementSibling.value = parseInt(x) + 1;                
          var productid = p.getAttribute('data-id');
          let formData = {
            'id':productid,
            'quantity': p.nextElementSibling.value
          }
          fetch("/cart/change.js",{
            method:'POST',
            headers: {
                'Content-Type':'application/json'
            },
            body:JSON.stringify(formData)
          })
          .then(response => {
            return response.json();
          })
          .then(response => {
            console.log(response);
            console.log("item updated");
          })
        }) 
      })
      let minus = document.querySelectorAll("li .minus");
      minus.forEach(m => {
        m.addEventListener("click",(e)=> {
          e.preventDefault();
          var y = m.previousElementSibling.value;
          m.previousElementSibling.value = parseInt(y) - 1;
          var productid= m.getAttribute('data-id');
          let formData = {
            'id': productid,
            'quantity' : m.previousElementSibling.value
          }
          fetch("/cart/change.js",{
            method : 'POST',
            headers : {
              'Content-Type':'application/json'
            },
            body : JSON.stringify(formData)
          })
          .then(response => {
            return response.json();
          })
          .then(response => {
            console.log(response);  
            console.log("item decreased");   
          })
        })
      })
    }
    closeBtn =() => {
      let close = document.querySelector(".sidecart-wrapper .close");
      close.addEventListener("click",(e)=> {
        e.preventDefault();
        document.querySelector('.sidecart-wrapper').classList.remove('active');
      })  
    }
    // submitEvent = (variant,qtyValue) => {
    //   let addToCart = document.querySelector('.product-form__buttons button');
    //   let sideCart = document.querySelector('.sidecart-wrapper');
    //   addToCart.addEventListener("click",(e) => {
    //     e.preventDefault();
    //     let formData = {
    //       'id': variant.id,
    //       'quantity': qtyValue
    //     };
    //     fetch(window.Shopify.routes.root +'cart/add.js', {
    //       method: 'POST',
    //       headers: {
    //         'Content-Type': 'application/json'
    //       },
    //       body: JSON.stringify(formData)
    //     })
    //     .then(response => {
    //       return response.json();
    //     })
    //     .then(response => {
    //       fetch(window.Shopify.routes.root +"/cart?view=view")
    //       .then(response => {
    //           return response.text();
    //       })
    //       .then(response => {
    //         document.querySelector(".cart-content").innerHTML = response;
    //       })
    //     })
    //     .then(response => {
    //       console.log(response);
    //       sideCart.classList.add('active');
    //     })
    //     .catch((error) => {
    //       console.error('Error:', error);
    //     });
    //   })
    //   document.addEventListener('mouseup', function(e) {
    //     if (!sideCart.contains(e.target)) {
    //       sideCart.classList.remove('active');
    //     }
    //   });
    // }
  }
  new AddtoCart;